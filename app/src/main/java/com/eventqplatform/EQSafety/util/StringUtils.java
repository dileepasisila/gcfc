package com.eventqplatform.EQSafety.util;

import java.util.ArrayList;
import java.util.List;

public class StringUtils {

    public static List<String> convertObjectListToStringList(List objects){
        List<String> stringList = new ArrayList<>();
        for (Object object : objects) {
            stringList.add(object.toString());
        }
        return stringList;
    }

    public static boolean isNotEmpty(String text){
        return text != null && text.length() > 0;
    }
}
