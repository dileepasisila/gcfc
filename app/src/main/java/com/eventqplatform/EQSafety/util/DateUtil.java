package com.eventqplatform.EQSafety.util;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    public static final String TAG = "DateUtil";

    public static final SimpleDateFormat MM_DD_YYYY = new SimpleDateFormat("MM/dd/yyyy");

    public static Date convertStringToDate(String dateString, SimpleDateFormat dateFormat){
        Date date = null;
        try {
            date = dateFormat.parse(dateString);
        }catch (Exception e){
            Log.e(TAG,"String to date conversion error..");
            e.printStackTrace();
        }
        return date;
    }

    public static Date convertStringToDate(String dateString){
        return convertStringToDate(dateString, MM_DD_YYYY);
    }

}
