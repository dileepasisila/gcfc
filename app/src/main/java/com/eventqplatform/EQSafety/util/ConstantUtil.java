package com.eventqplatform.EQSafety.util;

public class ConstantUtil {

    //Fragment Type
    public static final String FRAGMENT_HOME = "FRAGMENT_HOME";
    public static final String FRAGMENT_YOUR_CONTACT_INFO = "FRAGMENT_YOUR_CONTACT_INFO";
    public static final String FRAGMENT_TYPE_OF_INFORMATION = "FRAGMENT_TYPE_OF_INFORMATION";
    public static final String FRAGMENT_INFO_ABOUT_SUSPICIOUS_PERSON = "FRAGMENT_INFO_ABOUT_SUSPICIOUS_PERSON";
    public static final String FRAGMENT_INFO_ABOUT_SUSPICIOUS_VEHICLE = "FRAGMENT_INFO_ABOUT_SUSPICIOUS_VEHICLE";
    public static final String FRAGMENT_SUSPICIOUS_INCIDENT_OR_EVENT = "FRAGMENT_SUSPICIOUS_INCIDENT_OR_EVENT";

    //Name Code Pair Type
    public static final String CONTACT_TYPE = "contactType";
    public static final String EYE_COLOR_TYPE = "eyeColor";
    public static final String HAIR_COLOR_TYPE = "hairColor";
    public static final String INCIDENT_OR_EVENT_TYPE = "incidentOrEvent";

    //Name Bundle Keys
    public static final String YOUR_CONTACT_INFO_BUNDLE_KEY = "YOUR_CONTACT_INFO_BUNDLE_KEY";
    public static final String TYPE_OF_INFORMATION_BUNDLE_KEY = "TYPE_OF_INFORMATION_BUNDLE_KEY";
    public static final String SUSPICIOUS_PERSON_INFO_BUNDLE_KEY = "SUSPICIOUS_PERSON_INFO_BUNDLE_KEY";
    public static final String SUSPICIOUS_VEHICLE_INFO_BUNDLE_KEY = "SUSPICIOUS_VEHICLE_INFO_BUNDLE_KEY";
    public static final String SUSPICIOUS_INCIDENT_INFO_BUNDLE_KEY = "SUSPICIOUS_INCIDENT_INFO_BUNDLE_KEY";

    //GENDER TYPE
    public static final String MALE = "Male";
    public static final String FEMALE = "Female";

    //Firebase Node Key
    public static final String CONTACT_TYPES_FIREBASE_KEY = "contactTypes";
    public static final String HAIR_COLOR_FIREBASE_KEY = "hairColors";
    public static final String EYE_COLOR_FIREBASE_KEY = "eyeColors";
    public static final String TYPES_OF_INCIDENT_OR_EVENT_FIREBASE_KEY = "typesOfIncidentOrEvent";
    public static final String SUSPICIOUS_ACTIVITY_REPORT_FIREBASE_KEY = "suspiciousActivityReport";

    //Emergency Phone Num
    public static final String  EMERGENCY_PHONE_NUM = "911";

}
