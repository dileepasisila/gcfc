package com.eventqplatform.EQSafety.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.TypefaceProvider;
import com.eventqplatform.EQSafety.R;
import com.eventqplatform.EQSafety.firebase.dto.ContactType;
import com.eventqplatform.EQSafety.firebase.dto.EyeColor;
import com.eventqplatform.EQSafety.firebase.dto.HairColor;
import com.eventqplatform.EQSafety.firebase.dto.NameCodePair;
import com.eventqplatform.EQSafety.firebase.dto.TypeOfIncidentOrEvent;
import com.eventqplatform.EQSafety.fragment.HomeFragment;
import com.eventqplatform.EQSafety.fragment.SuspiciousIncidentFragment;
import com.eventqplatform.EQSafety.fragment.SuspiciousPersonInfoFragment;
import com.eventqplatform.EQSafety.fragment.SuspiciousVehicleInfoFragment;
import com.eventqplatform.EQSafety.fragment.TypeOfInformationFragment;
import com.eventqplatform.EQSafety.fragment.YourContactInfoFragment;
import com.eventqplatform.EQSafety.util.ConstantUtil;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HomeActivity extends AppCompatActivity{

    private static final String TAG = "HomeActivity";
    private FirebaseDatabase mFirebaseInstance;
    private DatabaseReference suspiciousActivityReportDbReference;
    private List<ContactType> contactTypeList;
    private List<EyeColor> eyeColorList;
    private List<HairColor> hairColorList;
    private List<TypeOfIncidentOrEvent> typeOfIncidentOrEventList;
    private ProgressDialog progressDialogForContactTypes;
    private ProgressDialog progressDialogForEyeColors;
    private ProgressDialog progressDialogForHairColors;
    private ProgressDialog progressDialogForTypeOfIncidentOrEvent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        init();
        mFirebaseInstance = FirebaseDatabase.getInstance();
        suspiciousActivityReportDbReference =
                mFirebaseInstance.getReference(ConstantUtil.SUSPICIOUS_ACTIVITY_REPORT_FIREBASE_KEY);
        loadData();
        TypefaceProvider.registerDefaultIconSets();
        selectFragment(ConstantUtil.FRAGMENT_HOME, savedInstanceState);

    }

    private void loadData() {

        progressDialogForContactTypes = ProgressDialog.show(
                this,
                "",
                "Loading Contatct Types .....",
                true,
                false,
                new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        progressDialogForContactTypes.dismiss();
                    }
                }
        );
        mFirebaseInstance.getReference(ConstantUtil.CONTACT_TYPES_FIREBASE_KEY)
                .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                int index = 0;
                for(DataSnapshot dst : dataSnapshot.getChildren()){
                    ContactType contactType = new ContactType();
                    contactType.setTypeName((String)dst.getValue());
                    contactType.setType(ConstantUtil.CONTACT_TYPE);
                    contactType.setId(index);
                    index ++;
                    contactTypeList.add(contactType);
                }
                if (progressDialogForContactTypes != null) {
                    progressDialogForContactTypes.dismiss();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "Failed to read contactTypes.", databaseError.toException());
                Toast.makeText(getApplicationContext(), "Failed to read contactTypes.",
                        Toast.LENGTH_SHORT).show();
                if (progressDialogForContactTypes != null) {
                    progressDialogForContactTypes.dismiss();
                }
            }
        });

        progressDialogForHairColors = ProgressDialog.show(
                this,
                "",
                "Loading Hair Colors .....",
                true,
                false,
                new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        progressDialogForHairColors.dismiss();
                    }
                }
        );

        mFirebaseInstance.getReference(ConstantUtil.HAIR_COLOR_FIREBASE_KEY)
                .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot dst : dataSnapshot.getChildren()){
                    HairColor hairColor = new HairColor();
                    String code = (String)((Map) dst.getValue()).get("code");
                    hairColor.setCode(code);
                    String name = (String)((Map) dst.getValue()).get("name");
                    hairColor.setName(name);
                    hairColor.setType(ConstantUtil.HAIR_COLOR_TYPE);
                    hairColorList.add(hairColor);
                }
                if (progressDialogForHairColors != null) {
                    progressDialogForHairColors.dismiss();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "Failed to read hairColors.", databaseError.toException());
                Toast.makeText(getApplicationContext(), "Failed to read Hair Colors.",
                        Toast.LENGTH_SHORT).show();
                if (progressDialogForHairColors != null) {
                    progressDialogForHairColors.dismiss();
                }
            }
        });

        progressDialogForEyeColors = ProgressDialog.show(
                this,
                "",
                "Loading Eye Colors .....",
                true,
                false,
                new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        progressDialogForEyeColors.dismiss();
                    }
                }
        );
        mFirebaseInstance.getReference(ConstantUtil.EYE_COLOR_FIREBASE_KEY)
                .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot dst : dataSnapshot.getChildren()){
                    EyeColor eyeColor = new EyeColor();
                    String code = (String)((Map) dst.getValue()).get("code");
                    eyeColor.setCode(code);
                    String name = (String)((Map) dst.getValue()).get("name");
                    eyeColor.setName(name);
                    eyeColor.setType(ConstantUtil.EYE_COLOR_TYPE);
                    eyeColorList.add(eyeColor);
                }

                if (progressDialogForEyeColors != null) {
                    progressDialogForEyeColors.dismiss();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "Failed to read eyeColors.", databaseError.toException());
                Toast.makeText(getApplicationContext(), "Failed to read Eye Colors.",
                        Toast.LENGTH_SHORT).show();
                if (progressDialogForEyeColors != null) {
                    progressDialogForEyeColors.dismiss();
                }
            }
        });

        progressDialogForTypeOfIncidentOrEvent = ProgressDialog.show(
                this,
                "",
                "Loading type of incident or events .....",
                true,
                false,
                new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        progressDialogForTypeOfIncidentOrEvent.dismiss();
                    }
                }
        );

        mFirebaseInstance.getReference(ConstantUtil.TYPES_OF_INCIDENT_OR_EVENT_FIREBASE_KEY)
                .addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot dst : dataSnapshot.getChildren()){
                    TypeOfIncidentOrEvent typeOfIncidentOrEvent = new TypeOfIncidentOrEvent();
                    String code = (String)((Map) dst.getValue()).get("code");
                    typeOfIncidentOrEvent.setCode(code);
                    String name = (String)((Map) dst.getValue()).get("name");
                    typeOfIncidentOrEvent.setName(name);
                    typeOfIncidentOrEvent.setType(ConstantUtil.INCIDENT_OR_EVENT_TYPE);
                    typeOfIncidentOrEventList.add(typeOfIncidentOrEvent);
                }
                if (progressDialogForTypeOfIncidentOrEvent != null) {
                    progressDialogForTypeOfIncidentOrEvent.dismiss();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "Failed to read typesOfIncidentOrEvent.", databaseError.toException());
                Toast.makeText(getApplicationContext(), "Failed to read typesOfIncidentOrEvent.",
                        Toast.LENGTH_SHORT).show();
                if (progressDialogForTypeOfIncidentOrEvent != null) {
                    progressDialogForTypeOfIncidentOrEvent.dismiss();
                }
            }
        });
    }

    private void init() {
        contactTypeList = new ArrayList<>();
        eyeColorList = new ArrayList<>();
        hairColorList = new ArrayList<>();
        typeOfIncidentOrEventList = new ArrayList<>();
    }

    public void selectFragment(String view, Bundle bundle) {
        Fragment fragment = null;
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        switch (view) {
            case ConstantUtil.FRAGMENT_HOME:
                fragment = new HomeFragment();
                fragment.setArguments(bundle);
                break;
            case ConstantUtil.FRAGMENT_YOUR_CONTACT_INFO:
                fragment = new YourContactInfoFragment();
                fragment.setArguments(bundle);
                break;
            case ConstantUtil.FRAGMENT_TYPE_OF_INFORMATION:
                fragment = new TypeOfInformationFragment();
                fragment.setArguments(bundle);
                break;
            case ConstantUtil.FRAGMENT_INFO_ABOUT_SUSPICIOUS_PERSON:
                fragment = new SuspiciousPersonInfoFragment();
                fragment.setArguments(bundle);
                break;
            case ConstantUtil.FRAGMENT_INFO_ABOUT_SUSPICIOUS_VEHICLE:
                fragment = new SuspiciousVehicleInfoFragment();
                fragment.setArguments(bundle);
                break;
            case ConstantUtil.FRAGMENT_SUSPICIOUS_INCIDENT_OR_EVENT:
                fragment = new SuspiciousIncidentFragment();
                fragment.setArguments(bundle);
                break;
            default:
                break;
        }
        displayFragment(fragment);
    }

    private void displayFragment(Fragment fragment) {
        if (fragment != null) {
            String backStackName = fragment.getClass().getName();
            FragmentManager fm = getSupportFragmentManager();
            if (fm.getBackStackEntryCount() > 1) {
                FragmentManager.BackStackEntry entry = fm.getBackStackEntryAt(fm.getBackStackEntryCount() - 2);
                if (fragment.getClass().getName().compareTo(entry.getName()) == 0) {
                    fm.popBackStackImmediate();
                }
            }
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(backStackName);
            fragmentTransaction.commit();
            ActionBar ab = getSupportActionBar();
            if (null != ab) {
                ab.setTitle("");
            } else {
                Log.e(TAG,"displayFragment getSupportActionBar returned null");
            }
            Log.d(TAG, "Backstack count: " + fm.getBackStackEntryCount());
        } else {
            Log.e(TAG,"null fragment argument passed to displayFragment");
        }
    }

    public List<ContactType> getContactTypeList() {
        return contactTypeList;
    }

    public List<EyeColor> getEyeColorList() {
        return eyeColorList;
    }

    public List<HairColor> getHairColorList() {
        return hairColorList;
    }

    public List<TypeOfIncidentOrEvent> getTypeOfIncidentOrEventList() {
        return typeOfIncidentOrEventList;
    }

    public void setDataToSpinner(List dataList, Spinner spinner, AdapterView.OnItemSelectedListener listener) {
        ArrayAdapter<NameCodePair> dataAdapter = new ArrayAdapter<NameCodePair>(this, R.layout.spinner_textview, dataList);
        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_textview);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(listener);
    }

    public DatabaseReference getSuspiciousActivityReportDbReference() {
        return suspiciousActivityReportDbReference;
    }
}
