package com.eventqplatform.EQSafety.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SuspiciousVehicleInfoDTO implements Parcelable {

    public static final Creator<SuspiciousVehicleInfoDTO> CREATOR = new Creator<SuspiciousVehicleInfoDTO>() {
        public SuspiciousVehicleInfoDTO createFromParcel(Parcel source) {
            return new SuspiciousVehicleInfoDTO(source);
        }

        public SuspiciousVehicleInfoDTO[] newArray(int size) {
            return new SuspiciousVehicleInfoDTO[size];
        }
    };

    public SuspiciousVehicleInfoDTO(){

    }

    protected SuspiciousVehicleInfoDTO(Parcel in) {
        this.vehicleColor = in.readString();
        this.vehicleYear = in.readString();
        this.vehicleMake = in.readString();
        this.vehicleBody = in.readString();
        this.licensePlateState = in.readString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.vehicleColor);
        parcel.writeString(this.vehicleYear);
        parcel.writeString(this.vehicleMake);
        parcel.writeString(this.vehicleBody);
        parcel.writeString(this.licensePlateState);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @SerializedName("vehicleColor")
    @Expose
    private String vehicleColor;
    @SerializedName("vehicleYear")
    @Expose
    private String vehicleYear;
    @SerializedName("vehicleMake")
    @Expose
    private String vehicleMake;
    @SerializedName("vehicleBody")
    @Expose
    private String vehicleBody;
    @SerializedName("licensePlateState")
    @Expose
    private String licensePlateState;

    public String getVehicleColor() {
        return vehicleColor;
    }

    public void setVehicleColor(String vehicleColor) {
        this.vehicleColor = vehicleColor;
    }

    public String getVehicleYear() {
        return vehicleYear;
    }

    public void setVehicleYear(String vehicleYear) {
        this.vehicleYear = vehicleYear;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleBody() {
        return vehicleBody;
    }

    public void setVehicleBody(String vehicleBody) {
        this.vehicleBody = vehicleBody;
    }

    public String getLicensePlateState() {
        return licensePlateState;
    }

    public void setLicensePlateState(String licensePlateState) {
        this.licensePlateState = licensePlateState;
    }
}
