package com.eventqplatform.EQSafety.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SuspiciousIncidentInfoDTO implements Parcelable{

    public static final Creator<SuspiciousIncidentInfoDTO> CREATOR = new Creator<SuspiciousIncidentInfoDTO>() {
        public SuspiciousIncidentInfoDTO createFromParcel(Parcel source) {
            return new SuspiciousIncidentInfoDTO(source);
        }

        public SuspiciousIncidentInfoDTO[] newArray(int size) {
            return new SuspiciousIncidentInfoDTO[size];
        }
    };

    public SuspiciousIncidentInfoDTO(){

    }

    protected SuspiciousIncidentInfoDTO(Parcel in) {
        this.suspiciousIncidentTypeCode = in.readString();
        this.suspiciousIncidentType = in.readString();
        this.seeOrHearEvidence = in.readString();
        this.date = in.readString();
        this.time = in.readString();
        this.location = in.readString();
        this.unusualOrSuspiciousReason = in.readString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.suspiciousIncidentTypeCode);
        parcel.writeString(this.suspiciousIncidentType);
        parcel.writeString(this.seeOrHearEvidence);
        parcel.writeString(this.date);
        parcel.writeString(this.time);
        parcel.writeString(this.location);
        parcel.writeString(this.unusualOrSuspiciousReason);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @SerializedName("suspiciousIncidentTypeCode")
    @Expose
    private String suspiciousIncidentTypeCode;
    @SerializedName("suspiciousIncidentType")
    @Expose
    private String suspiciousIncidentType;
    @SerializedName("seeOrHearEvidence")
    @Expose
    private String seeOrHearEvidence;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("unusualOrSuspiciousReason")
    @Expose
    private String unusualOrSuspiciousReason;

    public String getSeeOrHearEvidence() {
        return seeOrHearEvidence;
    }

    public void setSeeOrHearEvidence(String seeOrHearEvidence) {
        this.seeOrHearEvidence = seeOrHearEvidence;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUnusualOrSuspiciousReason() {
        return unusualOrSuspiciousReason;
    }

    public void setUnusualOrSuspiciousReason(String unusualOrSuspiciousReason) {
        this.unusualOrSuspiciousReason = unusualOrSuspiciousReason;
    }

    public String getSuspiciousIncidentTypeCode() {
        return suspiciousIncidentTypeCode;
    }

    public void setSuspiciousIncidentTypeCode(String suspiciousIncidentTypeCode) {
        this.suspiciousIncidentTypeCode = suspiciousIncidentTypeCode;
    }


    public String getSuspiciousIncidentType() {
        return suspiciousIncidentType;
    }

    public void setSuspiciousIncidentType(String suspiciousIncidentType) {
        this.suspiciousIncidentType = suspiciousIncidentType;
    }
}
