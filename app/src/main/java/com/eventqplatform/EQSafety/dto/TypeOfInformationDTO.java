package com.eventqplatform.EQSafety.dto;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class TypeOfInformationDTO implements Parcelable{

    public static final Creator<TypeOfInformationDTO> CREATOR = new Creator<TypeOfInformationDTO>() {
        public TypeOfInformationDTO createFromParcel(Parcel source) {
            return new TypeOfInformationDTO(source);
        }

        public TypeOfInformationDTO[] newArray(int size) {
            return new TypeOfInformationDTO[size];
        }
    };

    public TypeOfInformationDTO(){

    }

    protected TypeOfInformationDTO(Parcel in) {
        this.typeOfSuspiciousActivity = in.readString();
        this.informationObservedWay = in.readString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.typeOfSuspiciousActivity);
        parcel.writeString(this.informationObservedWay);
    }

    @SerializedName("typeOfSuspiciousActivity")
    @Expose
    private String typeOfSuspiciousActivity;
    @SerializedName("informationObservedWay")
    @Expose
    private String informationObservedWay;



    public String getTypeOfSuspiciousActivity() {
        return typeOfSuspiciousActivity;
    }

    public void setTypeOfSuspiciousActivity(String typeOfSuspiciousActivity) {
        this.typeOfSuspiciousActivity = typeOfSuspiciousActivity;
    }

    public String getInformationObservedWay() {
        return informationObservedWay;
    }

    public void setInformationObservedWay(String informationObservedWay) {
        this.informationObservedWay = informationObservedWay;
    }

    @Override
    public int describeContents() {
        return 0;
    }

}
