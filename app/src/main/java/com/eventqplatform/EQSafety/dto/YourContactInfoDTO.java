package com.eventqplatform.EQSafety.dto;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class YourContactInfoDTO implements Parcelable{

    @SerializedName("contactType")
    @Expose
    private String contactType;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("email")
    @Expose
    private String email;

    public static final Creator<YourContactInfoDTO> CREATOR = new Creator<YourContactInfoDTO>() {
        public YourContactInfoDTO createFromParcel(Parcel source) {
            return new YourContactInfoDTO(source);
        }

        public YourContactInfoDTO[] newArray(int size) {
            return new YourContactInfoDTO[size];
        }
    };

    protected YourContactInfoDTO(Parcel in) {
        this.contactType = in.readString();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.address = in.readString();
        this.phoneNumber = in.readString();
        this.email = in.readString();
    }

    public YourContactInfoDTO(){

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.contactType);
        parcel.writeString(this.firstName);
        parcel.writeString(this.lastName);
        parcel.writeString(this.address);
        parcel.writeString(this.phoneNumber);
        parcel.writeString(this.email);
    }


    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
