package com.eventqplatform.EQSafety.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SuspiciousPersonInfoDTO implements Parcelable{

    public static final Creator<SuspiciousPersonInfoDTO> CREATOR = new Creator<SuspiciousPersonInfoDTO>() {
        public SuspiciousPersonInfoDTO createFromParcel(Parcel source) {
            return new SuspiciousPersonInfoDTO(source);
        }

        public SuspiciousPersonInfoDTO[] newArray(int size) {
            return new SuspiciousPersonInfoDTO[size];
        }
    };

    public SuspiciousPersonInfoDTO(){

    }

    protected SuspiciousPersonInfoDTO(Parcel in) {
        this.name = in.readString();
        this.otherName = in.readString();
        this.address = in.readString();
        this.phoneNumber = in.readString();
        this.dob = in.readString();
        this.height = in.readString();
        this.weight = in.readString();
        this.hairColorCode = in.readString();
        this.hairColor = in.readString();
        this.eyeColorCode = in.readString();
        this.eyeColor = in.readString();
        this.raceEthnicity = in.readString();
        this.scarsMarksTattoosEtc = in.readString();
        this.gender = in.readString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.name);
        parcel.writeString(this.otherName);
        parcel.writeString(this.address);
        parcel.writeString(this.dob);
        parcel.writeString(this.height);
        parcel.writeString(this.weight);
        parcel.writeString(this.hairColorCode);
        parcel.writeString(this.hairColor);
        parcel.writeString(this.eyeColorCode);
        parcel.writeString(this.eyeColor);
        parcel.writeString(this.raceEthnicity);
        parcel.writeString(this.scarsMarksTattoosEtc);
        parcel.writeString(this.gender);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("otherName")
    @Expose
    private String otherName;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("height")
    @Expose
    private String height;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("hairColorCode")
    @Expose
    private String hairColorCode;
    @SerializedName("hairColor")
    @Expose
    private String hairColor;
    @SerializedName("eyeColorCode")
    @Expose
    private String eyeColorCode;
    @SerializedName("eyeColor")
    @Expose
    private String eyeColor;
    @SerializedName("raceEthnicity")
    @Expose
    private String raceEthnicity;
    @SerializedName("scarsMarksTattoosEtc")
    @Expose
    private String scarsMarksTattoosEtc;
    @SerializedName("gender")
    @Expose
    private String gender;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOtherName() {
        return otherName;
    }

    public void setOtherName(String otherName) {
        this.otherName = otherName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHairColorCode() {
        return hairColorCode;
    }

    public void setHairColorCode(String hairColorCode) {
        this.hairColorCode = hairColorCode;
    }

    public String getEyeColorCode() {
        return eyeColorCode;
    }

    public void setEyeColorCode(String eyeColorCode) {
        this.eyeColorCode = eyeColorCode;
    }

    public String getRaceEthnicity() {
        return raceEthnicity;
    }

    public void setRaceEthnicity(String raceEthnicity) {
        this.raceEthnicity = raceEthnicity;
    }

    public String getScarsMarksTattoosEtc() {
        return scarsMarksTattoosEtc;
    }

    public void setScarsMarksTattoosEtc(String scarsMarksTattoosEtc) {
        this.scarsMarksTattoosEtc = scarsMarksTattoosEtc;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHairColor() {
        return hairColor;
    }

    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    public String getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(String eyeColor) {
        this.eyeColor = eyeColor;
    }
}
