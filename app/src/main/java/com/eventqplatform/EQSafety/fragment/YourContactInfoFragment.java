package com.eventqplatform.EQSafety.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.eventqplatform.EQSafety.R;
import com.eventqplatform.EQSafety.activity.HomeActivity;
import com.eventqplatform.EQSafety.dto.YourContactInfoDTO;
import com.eventqplatform.EQSafety.firebase.dto.ContactType;
import com.eventqplatform.EQSafety.firebase.dto.NameCodePair;
import com.eventqplatform.EQSafety.util.ConstantUtil;
import com.eventqplatform.EQSafety.util.StringUtils;
import java.util.List;

public class YourContactInfoFragment extends BaseFragment implements View.OnClickListener,  AdapterView.OnItemSelectedListener{

    private View view;
    private Spinner contactTypeSelectionSpinner;
    private BootstrapEditText firstNameText;
    private BootstrapEditText lastNameText;
    private BootstrapEditText addressText;
    private BootstrapEditText phoneNumberText;
    private BootstrapEditText emailText;
    private BootstrapButton nextButton;
    private YourContactInfoDTO yourContactInfoDTO;
    private ContactType selectedContactType;


    @Override
    protected void initView(View view) {
        contactTypeSelectionSpinner = (Spinner)view.findViewById(R.id.contact_type_selection_spinner);
        HomeActivity homeActivity = (HomeActivity) getActivity();
        List<ContactType> contactTypes = homeActivity.getContactTypeList();
        homeActivity.setDataToSpinner(contactTypes, contactTypeSelectionSpinner, this);
        selectedContactType = contactTypes.get(0);
        firstNameText = (BootstrapEditText) view.findViewById(R.id.first_name_text);
        lastNameText = (BootstrapEditText) view.findViewById(R.id.last_name_text);
        addressText = (BootstrapEditText) view.findViewById(R.id.address_text);
        phoneNumberText = (BootstrapEditText) view.findViewById(R.id.phone_number_text);
        emailText = (BootstrapEditText) view.findViewById(R.id.email_text);
        nextButton = (BootstrapButton) view.findViewById(R.id.next_button);

        yourContactInfoDTO = new YourContactInfoDTO();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(view == null){
            view = inflater.inflate(R.layout.fragment_your_contact_info, container, false);
            initView(view);
            initListener();
            Bundle arguments = getArguments();
            if(arguments.containsKey(ConstantUtil.YOUR_CONTACT_INFO_BUNDLE_KEY)){
                yourContactInfoDTO = arguments.getParcelable(ConstantUtil.YOUR_CONTACT_INFO_BUNDLE_KEY);
                if(yourContactInfoDTO != null){
                    setData();
                }
            }
        }
        return view;
    }

    public void setData() {
        firstNameText.setText(yourContactInfoDTO.getFirstName());
        lastNameText.setText(yourContactInfoDTO.getLastName());
        addressText.setText(yourContactInfoDTO.getAddress());
        emailText.setText(yourContactInfoDTO.getEmail());
        phoneNumberText.setText(yourContactInfoDTO.getPhoneNumber());
        Adapter contactTypeAdapter = contactTypeSelectionSpinner.getAdapter();
        int contactTypeAdapterSize = contactTypeAdapter.getCount();
        for(int index = 0; index < contactTypeAdapterSize ; index++ ){
            ContactType contactType = (ContactType)contactTypeAdapter.getItem(index);
            if(contactType.getTypeName().equals(yourContactInfoDTO.getContactType())){
                contactTypeSelectionSpinner.setSelection(index);
                selectedContactType = contactType;
                break;
            }
        }
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        Bundle bundle = getArguments();
        switch (id) {
            case R.id.next_button:
                processInputData();
                boolean isSuccess = validateInputData();
                if(!isSuccess){
                    Toast.makeText(getContext(), "Data Validation Failed, Please, enter mandatory data.",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                bundle.putParcelable(ConstantUtil.YOUR_CONTACT_INFO_BUNDLE_KEY, yourContactInfoDTO);
                ((HomeActivity)getActivity()).selectFragment(ConstantUtil.FRAGMENT_TYPE_OF_INFORMATION, bundle);
                break;
            default:
                break;
        }
    }

    public boolean validateInputData() {
        boolean valid;
        valid = StringUtils.isNotEmpty(yourContactInfoDTO.getFirstName());
        if(!valid){
            return valid;
        }
        valid = StringUtils.isNotEmpty(yourContactInfoDTO.getLastName());
        if(!valid){
            return valid;
        }
        valid = StringUtils.isNotEmpty(yourContactInfoDTO.getAddress());
        if(!valid){
            return valid;
        }
        valid = StringUtils.isNotEmpty(yourContactInfoDTO.getEmail());
        if(!valid){
            return valid;
        }
        valid = StringUtils.isNotEmpty(yourContactInfoDTO.getPhoneNumber());
        if(!valid){
            return valid;
        }
        valid = StringUtils.isNotEmpty(yourContactInfoDTO.getContactType());
        if(!valid){
            return valid;
        }
        return valid;
    }

    public void processInputData() {
        yourContactInfoDTO.setContactType(selectedContactType.getTypeName());
        yourContactInfoDTO.setFirstName(firstNameText.getText().toString());
        yourContactInfoDTO.setLastName(lastNameText.getText().toString());
        yourContactInfoDTO.setAddress(addressText.getText().toString());
        yourContactInfoDTO.setEmail(emailText.getText().toString());
        yourContactInfoDTO.setPhoneNumber(phoneNumberText.getText().toString());
    }

    @Override
    protected void initListener() {
        nextButton.setOnClickListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position != 0) {
            NameCodePair nameCodePair = (NameCodePair) parent.getSelectedItem();
            if(nameCodePair.isContactType()) {
                selectedContactType = (ContactType) nameCodePair;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
