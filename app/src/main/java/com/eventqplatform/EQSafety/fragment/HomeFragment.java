package com.eventqplatform.EQSafety.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.eventqplatform.EQSafety.R;
import com.eventqplatform.EQSafety.activity.HomeActivity;
import com.eventqplatform.EQSafety.util.ConstantUtil;

public class HomeFragment extends BaseFragment implements View.OnClickListener{

    private BootstrapButton buttonStart;
    private BootstrapButton buttonDial;
    private View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(view == null){
            view = inflater.inflate(R.layout.fragment_home, container, false);
            initView(view);
            initListener();
        }
        return view;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        Bundle bundle = getArguments();
        if(bundle == null){
            bundle = new Bundle();
        }
        switch (id) {
            case R.id.button_start:
                ((HomeActivity)getActivity()).selectFragment(ConstantUtil.FRAGMENT_YOUR_CONTACT_INFO, bundle);
                break;
            case R.id.button_dial:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+ConstantUtil.EMERGENCY_PHONE_NUM));
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    protected void initView(View view) {
        buttonStart = (BootstrapButton) view.findViewById(R.id.button_start);
        buttonDial = (BootstrapButton) view.findViewById(R.id.button_dial);
    }

    @Override
    protected void initListener() {
        buttonStart.setOnClickListener(this);
        buttonDial.setOnClickListener(this);
    }

    @Override
    protected void processInputData() {

    }

    @Override
    protected void setData() {

    }
}
