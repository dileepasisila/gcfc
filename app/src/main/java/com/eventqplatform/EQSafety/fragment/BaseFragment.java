package com.eventqplatform.EQSafety.fragment;

import android.support.v4.app.Fragment;
import android.view.View;

public abstract class BaseFragment extends Fragment
{
    protected abstract void initView(View view);
    protected abstract void initListener();
    protected abstract void processInputData();
    protected abstract void setData();
    protected boolean validateInputData(){
        return true;
    }
}
