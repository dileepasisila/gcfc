package com.eventqplatform.EQSafety.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.eventqplatform.EQSafety.R;
import com.eventqplatform.EQSafety.activity.HomeActivity;
import com.eventqplatform.EQSafety.dto.TypeOfInformationDTO;
import com.eventqplatform.EQSafety.util.ConstantUtil;
import com.eventqplatform.EQSafety.util.StringUtils;

public class TypeOfInformationFragment extends BaseFragment implements View.OnClickListener {

    private View view;
    private BootstrapEditText typeOfSuspiciousActivityText;
    private BootstrapEditText infoForObtainMethodText;
    private BootstrapButton nextButton;
    private BootstrapButton prevButton;
    private TypeOfInformationDTO typeOfInformationDTO;

    @Override
    protected void initView(View view) {
        typeOfSuspiciousActivityText = (BootstrapEditText) view.findViewById(R.id.type_of_suspicious_activity_text);
        infoForObtainMethodText = (BootstrapEditText) view.findViewById(R.id.suspicious_information_text);
        nextButton = (BootstrapButton) view.findViewById(R.id.next_button);
        prevButton = (BootstrapButton) view.findViewById(R.id.previous_button);
        typeOfInformationDTO = new TypeOfInformationDTO();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        Bundle bundle = getArguments();
        switch (id) {
            case R.id.next_button:
                processInputData();
                boolean isSuccess = validateInputData();
                if(!isSuccess){
                    Toast.makeText(getContext(), "Data Validation Failed, Please, enter mandatory data.",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                bundle.putParcelable(ConstantUtil.TYPE_OF_INFORMATION_BUNDLE_KEY, typeOfInformationDTO);
                ((HomeActivity)getActivity()).selectFragment(ConstantUtil.FRAGMENT_INFO_ABOUT_SUSPICIOUS_PERSON, bundle);
                break;
            case R.id.previous_button:
                processInputData();
                bundle.putParcelable(ConstantUtil.TYPE_OF_INFORMATION_BUNDLE_KEY, typeOfInformationDTO);
                ((HomeActivity)getActivity()).selectFragment(ConstantUtil.FRAGMENT_YOUR_CONTACT_INFO, bundle);
                break;
            default:
                break;
        }
    }

    public boolean validateInputData() {
        boolean valid;
        valid = StringUtils.isNotEmpty(typeOfInformationDTO.getInformationObservedWay());
        if(!valid){
            return valid;
        }
        valid = StringUtils.isNotEmpty(typeOfInformationDTO.getTypeOfSuspiciousActivity());
        if(!valid){
            return valid;
        }
        return valid;
    }

    public void processInputData() {
        typeOfInformationDTO.setTypeOfSuspiciousActivity(typeOfSuspiciousActivityText.getText().toString());
        typeOfInformationDTO.setInformationObservedWay(infoForObtainMethodText.getText().toString());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(view == null){
            view = inflater.inflate(R.layout.fragment_type_of_information, container, false);
            initView(view);
            initListener();
            Bundle arguments = getArguments();
            if(arguments.containsKey(ConstantUtil.TYPE_OF_INFORMATION_BUNDLE_KEY)){
                typeOfInformationDTO = arguments.getParcelable(ConstantUtil.TYPE_OF_INFORMATION_BUNDLE_KEY);
                if(typeOfInformationDTO != null){
                    setData();
                }
            }
        }
        return view;
    }

    public void setData() {
        typeOfSuspiciousActivityText.setText(typeOfInformationDTO.getTypeOfSuspiciousActivity());
        infoForObtainMethodText.setText(typeOfInformationDTO.getInformationObservedWay());
    }


    @Override
    protected void initListener() {
        nextButton.setOnClickListener(this);
        prevButton.setOnClickListener(this);
    }
}
