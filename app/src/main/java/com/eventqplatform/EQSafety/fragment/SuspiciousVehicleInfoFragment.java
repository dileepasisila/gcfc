package com.eventqplatform.EQSafety.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.eventqplatform.EQSafety.R;
import com.eventqplatform.EQSafety.activity.HomeActivity;
import com.eventqplatform.EQSafety.dto.SuspiciousVehicleInfoDTO;
import com.eventqplatform.EQSafety.util.ConstantUtil;

public class SuspiciousVehicleInfoFragment extends BaseFragment implements View.OnClickListener {

    private View view;
    private BootstrapEditText vehicleColorText;
    private BootstrapEditText vehicleYearText;
    private BootstrapEditText vehicleMakeText;
    private BootstrapEditText vehicleBodyText;
    private BootstrapEditText licensePlateStatText;
    private BootstrapButton nextButton;
    private BootstrapButton prevButton;
    private SuspiciousVehicleInfoDTO suspiciousVehicleInfoDTO;

    @Override
    protected void initView(View view) {
        vehicleColorText = (BootstrapEditText) view.findViewById(R.id.vehicle_color_text);
        vehicleYearText = (BootstrapEditText) view.findViewById(R.id.vehicle_year_text);
        vehicleMakeText = (BootstrapEditText) view.findViewById(R.id.vehicle_make_text);
        vehicleBodyText = (BootstrapEditText) view.findViewById(R.id.vehicle_body_text);
        licensePlateStatText = (BootstrapEditText) view.findViewById(R.id.license_plate_state_text);
        nextButton = (BootstrapButton) view.findViewById(R.id.next_button);
        prevButton = (BootstrapButton) view.findViewById(R.id.previous_button);
        suspiciousVehicleInfoDTO = new SuspiciousVehicleInfoDTO();
    }

    public void processInputData() {
        suspiciousVehicleInfoDTO.setVehicleColor(vehicleColorText.getText().toString());
        suspiciousVehicleInfoDTO.setVehicleYear(vehicleYearText.getText().toString());
        suspiciousVehicleInfoDTO.setVehicleMake(vehicleMakeText.getText().toString());
        suspiciousVehicleInfoDTO.setVehicleBody(vehicleBodyText.getText().toString());
        suspiciousVehicleInfoDTO.setLicensePlateState(licensePlateStatText.getText().toString());
    }

    public void setData() {
        vehicleColorText.setText(suspiciousVehicleInfoDTO.getVehicleColor());
        vehicleYearText.setText(suspiciousVehicleInfoDTO.getVehicleYear());
        vehicleMakeText.setText(suspiciousVehicleInfoDTO.getVehicleMake());
        vehicleBodyText.setText(suspiciousVehicleInfoDTO.getVehicleBody());
        licensePlateStatText.setText(suspiciousVehicleInfoDTO.getLicensePlateState());
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        Bundle bundle = getArguments();
        switch (id) {
            case R.id.next_button:
                processInputData();
                boolean isSuccess = validateInputData();
                if(!isSuccess){
                    Toast.makeText(getContext(), "Data Validation Failed, Please, enter mandatory data.",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                bundle.putParcelable(ConstantUtil.SUSPICIOUS_VEHICLE_INFO_BUNDLE_KEY, suspiciousVehicleInfoDTO);
                ((HomeActivity)getActivity()).selectFragment(ConstantUtil.FRAGMENT_SUSPICIOUS_INCIDENT_OR_EVENT, bundle);
                break;
            case R.id.previous_button:
                processInputData();
                bundle.putParcelable(ConstantUtil.SUSPICIOUS_VEHICLE_INFO_BUNDLE_KEY, suspiciousVehicleInfoDTO);
                ((HomeActivity)getActivity()).selectFragment(ConstantUtil.FRAGMENT_INFO_ABOUT_SUSPICIOUS_PERSON, bundle);
                break;
            default:
                break;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(view == null){
            view = inflater.inflate(R.layout.fragment_information_about_suspicious_person_vehicle, container, false);
            initView(view);
            initListener();
            Bundle arguments = getArguments();
            if(arguments.containsKey(ConstantUtil.SUSPICIOUS_VEHICLE_INFO_BUNDLE_KEY)){
                suspiciousVehicleInfoDTO = arguments.getParcelable(ConstantUtil.SUSPICIOUS_VEHICLE_INFO_BUNDLE_KEY);
                if(suspiciousVehicleInfoDTO != null){
                    setData();
                }
            }
        }
        return view;
    }

    @Override
    protected void initListener() {
        nextButton.setOnClickListener(this);
        prevButton.setOnClickListener(this);
    }
}
