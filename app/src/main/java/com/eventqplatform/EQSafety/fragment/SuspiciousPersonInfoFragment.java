package com.eventqplatform.EQSafety.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.eventqplatform.EQSafety.R;
import com.eventqplatform.EQSafety.activity.HomeActivity;
import com.eventqplatform.EQSafety.dto.SuspiciousPersonInfoDTO;
import com.eventqplatform.EQSafety.firebase.dto.EyeColor;
import com.eventqplatform.EQSafety.firebase.dto.HairColor;
import com.eventqplatform.EQSafety.firebase.dto.NameCodePair;
import com.eventqplatform.EQSafety.util.ConstantUtil;
import com.eventqplatform.EQSafety.util.DateUtil;
import com.eventqplatform.EQSafety.util.StringUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SuspiciousPersonInfoFragment extends BaseFragment implements View.OnClickListener,  AdapterView.OnItemSelectedListener{

    private View view;
    private HomeActivity homeActivity;
    private BootstrapEditText nameText;
    private BootstrapEditText otherNameText;
    private BootstrapEditText addressText;
    private BootstrapEditText phoneNumberText;
    private BootstrapEditText dobText;
    private BootstrapButton dobSelectButton;
    private BootstrapEditText heightText;
    private BootstrapEditText weightText;
    private Spinner hairSelectionSpinner;
    private Spinner eyeSelectionSpinner;
    private BootstrapEditText raceEthnicityText;
    private BootstrapEditText scarsMarksTattoosEtcText;
    private BootstrapButton nextButton;
    private BootstrapButton prevButton;
    private EyeColor selectedEyeColor;
    private HairColor selectedHairColor;
    private SuspiciousPersonInfoDTO suspiciousPersonInfoDTO;
    private String selectedGenderOption;
    private RadioGroup genderSelectionRadioGroup;
    private RadioButton maleRadioButton;
    private RadioButton femaleRadioButton;
    private DatePickerDialog dobPickerDialog;
    private Date dob;


    @Override
    protected void initView(View view) {
        homeActivity = (HomeActivity)getActivity();
        nameText = (BootstrapEditText) view.findViewById(R.id.name_text);
        otherNameText = (BootstrapEditText) view.findViewById(R.id.other_name_text);
        addressText = (BootstrapEditText) view.findViewById(R.id.address_text);
        phoneNumberText = (BootstrapEditText) view.findViewById(R.id.phone_number_text);
        dobText = (BootstrapEditText) view.findViewById(R.id.dob_text);
        dobSelectButton = (BootstrapButton) view.findViewById(R.id.dob_select_button);
        heightText = (BootstrapEditText) view.findViewById(R.id.height_text);
        weightText = (BootstrapEditText) view.findViewById(R.id.weight_text);
        hairSelectionSpinner = (Spinner) view.findViewById(R.id.hair_selection_spinner);
        List<HairColor> hairColorList = ((HomeActivity) getActivity()).getHairColorList();
        homeActivity.setDataToSpinner(hairColorList, hairSelectionSpinner, this);
        selectedHairColor = hairColorList.get(0);
        eyeSelectionSpinner = (Spinner) view.findViewById(R.id.eye_selection_spinner);
        List<EyeColor> eyeColorList = ((HomeActivity) getActivity()).getEyeColorList();
        homeActivity.setDataToSpinner(eyeColorList, eyeSelectionSpinner, this);
        selectedEyeColor = eyeColorList.get(0);
        raceEthnicityText = (BootstrapEditText) view.findViewById(R.id.race_ethnicity_text);
        scarsMarksTattoosEtcText = (BootstrapEditText) view.findViewById(R.id.scars_marks_tattoos_etc_text);
        genderSelectionRadioGroup = (RadioGroup) view.findViewById(R.id.gender_selection_radio_group);
        maleRadioButton = (RadioButton) view.findViewById(R.id.male_radio);
        femaleRadioButton = (RadioButton) view.findViewById(R.id.female_radio);
        nextButton = (BootstrapButton) view.findViewById(R.id.next_button);
        prevButton = (BootstrapButton) view.findViewById(R.id.previous_button);
        suspiciousPersonInfoDTO = new SuspiciousPersonInfoDTO();
    }

    public void setupDobPickerDialog(final SuspiciousPersonInfoFragment fragment){
        Calendar cal = Calendar.getInstance();
        String dobStringVal = dobText.getText().toString();
        if(StringUtils.isNotEmpty(dobStringVal)){
            Date date = DateUtil.convertStringToDate(dobStringVal);
            cal.setTime(date);
        }
        dobText.setText(DateUtil.MM_DD_YYYY.format(cal.getTime()));
        fragment.setDob(cal.getTime());
        int mYear = cal.get(Calendar.YEAR);
        int mMonth = cal.get(Calendar.MONTH);
        int mDay = cal.get(Calendar.DAY_OF_MONTH);
        dobPickerDialog = new DatePickerDialog(getContext()
                ,new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, month);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                fragment.setDob(cal.getTime());
                dobText.setText(DateUtil.MM_DD_YYYY.format(cal.getTime()));
            }
        },mYear, mMonth, mDay);
    }

    private void setDob(Date dob){
        this.dob = dob;
    }

    public void processInputData() {
        suspiciousPersonInfoDTO.setName(nameText.getText().toString());
        suspiciousPersonInfoDTO.setOtherName(otherNameText.getText().toString());
        suspiciousPersonInfoDTO.setAddress(addressText.getText().toString());
        suspiciousPersonInfoDTO.setPhoneNumber(phoneNumberText.getText().toString());
        suspiciousPersonInfoDTO.setDob(dobText.getText().toString());
        suspiciousPersonInfoDTO.setHeight(heightText.getText().toString());
        suspiciousPersonInfoDTO.setWeight(weightText.getText().toString());
        suspiciousPersonInfoDTO.setHairColorCode(selectedHairColor.getCode());
        suspiciousPersonInfoDTO.setHairColor(selectedHairColor.toString());
        suspiciousPersonInfoDTO.setEyeColorCode(selectedEyeColor.getCode());
        suspiciousPersonInfoDTO.setEyeColor(selectedEyeColor.toString());
        suspiciousPersonInfoDTO.setScarsMarksTattoosEtc(scarsMarksTattoosEtcText.getText().toString());
        suspiciousPersonInfoDTO.setRaceEthnicity(raceEthnicityText.getText().toString());
        suspiciousPersonInfoDTO.setGender(selectedGenderOption);
    }

    public void setData() {
        nameText.setText(suspiciousPersonInfoDTO.getName());
        otherNameText.setText(suspiciousPersonInfoDTO.getOtherName());
        addressText.setText(suspiciousPersonInfoDTO.getAddress());
        phoneNumberText.setText(suspiciousPersonInfoDTO.getPhoneNumber());
        dobText.setText(suspiciousPersonInfoDTO.getDob());
        heightText.setText(suspiciousPersonInfoDTO.getHeight());
        weightText.setText(suspiciousPersonInfoDTO.getWeight());
        Adapter hairColorAdapter = hairSelectionSpinner.getAdapter();
        int hairColorAdapterSize = hairColorAdapter.getCount();
        for(int index = 0; index < hairColorAdapterSize ; index++ ){
            HairColor hairColor = (HairColor)hairColorAdapter.getItem(index);
            if(hairColor.getCode().equals(suspiciousPersonInfoDTO.getHairColorCode())){
                hairSelectionSpinner.setSelection(index);
                selectedHairColor = hairColor;
                break;
            }
        }
        Adapter eyeColorAdapter = eyeSelectionSpinner.getAdapter();
        int eyeColorAdapterSize = eyeColorAdapter.getCount();
        for(int index = 0; index < eyeColorAdapterSize ; index++ ){
            EyeColor eyeColor = (EyeColor)eyeColorAdapter.getItem(index);
            if(eyeColor.getCode().equals(suspiciousPersonInfoDTO.getEyeColorCode())){
                eyeSelectionSpinner.setSelection(index);
                selectedEyeColor = eyeColor;
                break;
            }
        }
        scarsMarksTattoosEtcText.setText(suspiciousPersonInfoDTO.getScarsMarksTattoosEtc());
        raceEthnicityText.setText(suspiciousPersonInfoDTO.getRaceEthnicity());
        int childCount = genderSelectionRadioGroup.getChildCount();
        for(int index = 0; index < childCount ; index++ ){
            View radioView = genderSelectionRadioGroup.getChildAt(index);
            RadioButton radioButton = (RadioButton) radioView;
            String radioText = radioButton.getText().toString();
            if(radioText.equals(suspiciousPersonInfoDTO.getGender())){
                radioButton.setChecked(true);
                selectedGenderOption = radioText;
                break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        Bundle bundle = getArguments();
        switch (id) {
            case R.id.next_button:
                processInputData();
                boolean isSuccess = validateInputData();
                if(!isSuccess){
                    Toast.makeText(getContext(), "Data Validation Failed, Please, enter mandatory data.",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                bundle.putParcelable(ConstantUtil.SUSPICIOUS_PERSON_INFO_BUNDLE_KEY, suspiciousPersonInfoDTO);
                ((HomeActivity)getActivity()).selectFragment(ConstantUtil.FRAGMENT_INFO_ABOUT_SUSPICIOUS_VEHICLE, bundle);
                break;
            case R.id.previous_button:
                processInputData();
                bundle.putParcelable(ConstantUtil.SUSPICIOUS_PERSON_INFO_BUNDLE_KEY, suspiciousPersonInfoDTO);
                ((HomeActivity)getActivity()).selectFragment(ConstantUtil.FRAGMENT_TYPE_OF_INFORMATION, bundle);
                break;
            case R.id.dob_select_button:
                dobPickerDialog.show();
                break;
            default:
                break;
        }
        if(view instanceof RadioButton){
            selectedGenderOption = ((RadioButton) view).getText().toString();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(view == null){
            view = inflater.inflate(R.layout.fragment_information_about_suspicious_person, container, false);
            initView(view);
            initListener();
            Bundle arguments = getArguments();
            if(arguments.containsKey(ConstantUtil.SUSPICIOUS_PERSON_INFO_BUNDLE_KEY)){
                suspiciousPersonInfoDTO = arguments.getParcelable(ConstantUtil.SUSPICIOUS_PERSON_INFO_BUNDLE_KEY);
                if(suspiciousPersonInfoDTO != null){
                    setData();
                }
            }
            setupDobPickerDialog(this);
        }
        return view;
    }

    @Override
    protected void initListener() {
        nextButton.setOnClickListener(this);
        prevButton.setOnClickListener(this);
        maleRadioButton.setOnClickListener(this);
        femaleRadioButton.setOnClickListener(this);
        dobSelectButton.setOnClickListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position != 0) {
            NameCodePair nameCodePair = (NameCodePair) parent.getSelectedItem();
            if(nameCodePair.isEyeColorType()){
                selectedEyeColor = (EyeColor)nameCodePair;
            }else if(nameCodePair.isHairColorType()){
                selectedHairColor = (HairColor) nameCodePair;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
