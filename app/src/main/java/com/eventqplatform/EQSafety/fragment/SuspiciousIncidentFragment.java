package com.eventqplatform.EQSafety.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;

import com.beardedhen.androidbootstrap.BootstrapEditText;
import com.eventqplatform.EQSafety.R;
import com.eventqplatform.EQSafety.activity.HomeActivity;
import com.eventqplatform.EQSafety.dto.SuspiciousIncidentInfoDTO;
import com.eventqplatform.EQSafety.dto.SuspiciousPersonInfoDTO;
import com.eventqplatform.EQSafety.dto.SuspiciousVehicleInfoDTO;
import com.eventqplatform.EQSafety.dto.TypeOfInformationDTO;
import com.eventqplatform.EQSafety.dto.YourContactInfoDTO;
import com.eventqplatform.EQSafety.firebase.dto.NameCodePair;
import com.eventqplatform.EQSafety.firebase.dto.TypeOfIncidentOrEvent;
import com.eventqplatform.EQSafety.firebase.dto.save.Contacts;
import com.eventqplatform.EQSafety.firebase.dto.save.InformationAboutSuspuciousPerson;
import com.eventqplatform.EQSafety.firebase.dto.save.SuspiciousActivityReport;
import com.eventqplatform.EQSafety.firebase.dto.save.SuspiciousIncidentOrEvent;
import com.eventqplatform.EQSafety.firebase.dto.save.TypeOfInformation;
import com.eventqplatform.EQSafety.firebase.dto.save.VehicleInformation;
import com.eventqplatform.EQSafety.util.ConstantUtil;
import com.eventqplatform.EQSafety.util.DateUtil;
import com.eventqplatform.EQSafety.util.StringUtils;
import com.google.firebase.database.DatabaseReference;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SuspiciousIncidentFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private View view;
    private HomeActivity homeActivity;
    private BootstrapButton prevButton;
    private BootstrapButton saveButton;
    private Spinner incidentOrEventSpinner;
    private BootstrapEditText whatDidYouSeeOrHearText;
    private BootstrapEditText dateText;
    private BootstrapButton dateSelectButton;
    private BootstrapEditText timeText;
    private BootstrapEditText locationText;
    private BootstrapEditText unusualOrSuspiciousReasonText;
    private TypeOfIncidentOrEvent selectedTypeOfIncidentOrEvent;
    private SuspiciousIncidentInfoDTO suspiciousIncidentInfoDTO;
    private DatePickerDialog datePickerDialog;
    private Date date;
    private ProgressDialog progressDialogForSaveDataToFirebase;

    @Override
    protected void initView(View view) {
        homeActivity = (HomeActivity)getActivity();
        incidentOrEventSpinner = (Spinner) view.findViewById(R.id.type_of_suspicious_incident_or_event_spinner);
        List<TypeOfIncidentOrEvent> typeOfIncidentOrEvents = ((HomeActivity) getActivity()).getTypeOfIncidentOrEventList();
        homeActivity.setDataToSpinner(typeOfIncidentOrEvents, incidentOrEventSpinner, this);
        selectedTypeOfIncidentOrEvent = typeOfIncidentOrEvents.get(0);
        whatDidYouSeeOrHearText = (BootstrapEditText) view.findViewById(R.id.what_did_you_see_or_hear_text);
        dateText = (BootstrapEditText) view.findViewById(R.id.date_text);
        dateSelectButton = (BootstrapButton) view.findViewById(R.id.date_select_button);
        timeText = (BootstrapEditText) view.findViewById(R.id.time_text);
        locationText = (BootstrapEditText) view.findViewById(R.id.location_text);
        unusualOrSuspiciousReasonText = (BootstrapEditText) view.findViewById(R.id.unusual_or_suspicious_reason_text);
        prevButton = (BootstrapButton) view.findViewById(R.id.prev_button);
        saveButton = (BootstrapButton) view.findViewById(R.id.submit_sar_form_button);
        suspiciousIncidentInfoDTO = new SuspiciousIncidentInfoDTO();
    }


    public void setupDatePickerDialog(final SuspiciousIncidentFragment fragment){
        Calendar cal = Calendar.getInstance();
        String dobStringVal = dateText.getText().toString();
        if(StringUtils.isNotEmpty(dobStringVal)){
            Date date = DateUtil.convertStringToDate(dobStringVal);
            cal.setTime(date);
        }
        dateText.setText(DateUtil.MM_DD_YYYY.format(cal.getTime()));
        fragment.setDob(cal.getTime());
        int mYear = cal.get(Calendar.YEAR);
        int mMonth = cal.get(Calendar.MONTH);
        int mDay = cal.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(getContext()
                ,new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.YEAR, year);
                cal.set(Calendar.MONTH, month);
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                fragment.setDob(cal.getTime());
                dateText.setText(DateUtil.MM_DD_YYYY.format(cal.getTime()));
            }
        },mYear, mMonth, mDay);
    }

    private void setDob(Date date){
        this.date = date;
    }

    public void processInputData() {
        suspiciousIncidentInfoDTO.setSuspiciousIncidentTypeCode(selectedTypeOfIncidentOrEvent.getCode());
        suspiciousIncidentInfoDTO.setSuspiciousIncidentType(selectedTypeOfIncidentOrEvent.toString());
        suspiciousIncidentInfoDTO.setSeeOrHearEvidence(whatDidYouSeeOrHearText.getText().toString());
        suspiciousIncidentInfoDTO.setDate(dateText.getText().toString());
        suspiciousIncidentInfoDTO.setTime(timeText.getText().toString());
        suspiciousIncidentInfoDTO.setLocation(locationText.getText().toString());
        suspiciousIncidentInfoDTO.setUnusualOrSuspiciousReason(unusualOrSuspiciousReasonText.getText().toString());
    }

    public void setData() {
        Adapter eventAdapter = incidentOrEventSpinner.getAdapter();
        int eventAdapterSize = eventAdapter.getCount();
        for(int index = 0; index < eventAdapterSize ; index++ ){
            TypeOfIncidentOrEvent typeOfIncidentOrEvent = (TypeOfIncidentOrEvent)eventAdapter.getItem(index);
            if(typeOfIncidentOrEvent.getCode().equals(suspiciousIncidentInfoDTO.getSuspiciousIncidentTypeCode())){
                incidentOrEventSpinner.setSelection(index);
                selectedTypeOfIncidentOrEvent = typeOfIncidentOrEvent;
                break;
            }
        }
        whatDidYouSeeOrHearText.setText(suspiciousIncidentInfoDTO.getSeeOrHearEvidence());
        dateText.setText(suspiciousIncidentInfoDTO.getDate());
        timeText.setText(suspiciousIncidentInfoDTO.getTime());
        locationText.setText(suspiciousIncidentInfoDTO.getLocation());
        unusualOrSuspiciousReasonText.setText(suspiciousIncidentInfoDTO.getUnusualOrSuspiciousReason());
    }

    public boolean validateInputData() {
        boolean valid;
        valid = StringUtils.isNotEmpty(suspiciousIncidentInfoDTO.getSuspiciousIncidentTypeCode());
        if(!valid){
            return valid;
        }
        valid = StringUtils.isNotEmpty(suspiciousIncidentInfoDTO.getSeeOrHearEvidence());
        if(!valid){
            return valid;
        }
        valid = StringUtils.isNotEmpty(suspiciousIncidentInfoDTO.getDate());
        if(!valid){
            return valid;
        }
        valid = StringUtils.isNotEmpty(suspiciousIncidentInfoDTO.getTime());
        if(!valid){
            return valid;
        }
        valid = StringUtils.isNotEmpty(suspiciousIncidentInfoDTO.getLocation());
        if(!valid){
            return valid;
        }
        valid = StringUtils.isNotEmpty(suspiciousIncidentInfoDTO.getUnusualOrSuspiciousReason());
        if(!valid){
            return valid;
        }
        return valid;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        Bundle bundle = getArguments();
        switch (id) {
            case R.id.submit_sar_form_button:
                processInputData();
                boolean isSuccess = validateInputData();
                if(!isSuccess){
                    Toast.makeText(getContext(), "Data Validation Failed, Please, enter mandatory data.",
                            Toast.LENGTH_SHORT).show();
                    return;
                }
                bundle.putParcelable(ConstantUtil.SUSPICIOUS_INCIDENT_INFO_BUNDLE_KEY, suspiciousIncidentInfoDTO);
                processResult(bundle);
                break;
            case R.id.prev_button:
                processInputData();
                bundle.putParcelable(ConstantUtil.SUSPICIOUS_INCIDENT_INFO_BUNDLE_KEY, suspiciousIncidentInfoDTO);
                ((HomeActivity)getActivity()).selectFragment(ConstantUtil.FRAGMENT_INFO_ABOUT_SUSPICIOUS_VEHICLE, bundle);
                break;
            case R.id.date_select_button:
                datePickerDialog.show();
                break;
            default:
                break;
        }
    }

    private void processResult(Bundle bundle) {
        progressDialogForSaveDataToFirebase = ProgressDialog.show(
                getContext(),
                "",
                "Data Saving .....",
                true,
                false,
                new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        progressDialogForSaveDataToFirebase.dismiss();
                    }
                }
        );
        try{
            sendDataToFirebase(bundle);
            Toast.makeText(getContext(), "Data Saved Successfully...",
                    Toast.LENGTH_SHORT).show();
            clearBundleData(bundle);
            ((HomeActivity)getActivity()).selectFragment(ConstantUtil.FRAGMENT_HOME, bundle);
        }catch ( Exception e){
            Toast.makeText(getContext(), "Data Saved failed, please, check your network connection...",
                    Toast.LENGTH_SHORT).show();
        }finally {
            if (progressDialogForSaveDataToFirebase != null) {
                progressDialogForSaveDataToFirebase.dismiss();
            }
        }
    }

    private void sendDataToFirebase(Bundle bundle) {
        SuspiciousActivityReport suspiciousActivityReport = buildFirebaseTransterObject(bundle);
        DatabaseReference suspiciousActivityReportDbReference
                = ((HomeActivity) getActivity()).getSuspiciousActivityReportDbReference();
        String uniqueId = suspiciousActivityReportDbReference.push().getKey();
        suspiciousActivityReportDbReference.child(uniqueId).setValue(suspiciousActivityReport);
    }

    private void clearBundleData(Bundle bundle) {
        bundle.remove(ConstantUtil.YOUR_CONTACT_INFO_BUNDLE_KEY);
        bundle.remove(ConstantUtil.TYPE_OF_INFORMATION_BUNDLE_KEY);
        bundle.remove(ConstantUtil.SUSPICIOUS_PERSON_INFO_BUNDLE_KEY);
        bundle.remove(ConstantUtil.SUSPICIOUS_VEHICLE_INFO_BUNDLE_KEY);
        bundle.remove(ConstantUtil.SUSPICIOUS_INCIDENT_INFO_BUNDLE_KEY);
    }

    private SuspiciousActivityReport buildFirebaseTransterObject(Bundle bundle) {
        SuspiciousActivityReport suspiciousActivityReport = new SuspiciousActivityReport();
        if(bundle.containsKey(ConstantUtil.YOUR_CONTACT_INFO_BUNDLE_KEY)){
            YourContactInfoDTO yourContactInfoDTO = bundle.getParcelable(ConstantUtil.YOUR_CONTACT_INFO_BUNDLE_KEY);
            if(yourContactInfoDTO != null){
                Contacts contacts = new Contacts(yourContactInfoDTO);
                suspiciousActivityReport.setContacts(contacts);
            }
        }
        if(bundle.containsKey(ConstantUtil.TYPE_OF_INFORMATION_BUNDLE_KEY)){
            TypeOfInformationDTO typeOfInformationDTO = bundle.getParcelable(ConstantUtil.TYPE_OF_INFORMATION_BUNDLE_KEY);
            if(typeOfInformationDTO != null){
                TypeOfInformation typeOfInformation = new TypeOfInformation(typeOfInformationDTO);
                suspiciousActivityReport.setTypeOfInformation(typeOfInformation);
            }
        }
        if(bundle.containsKey(ConstantUtil.SUSPICIOUS_PERSON_INFO_BUNDLE_KEY)){
            SuspiciousPersonInfoDTO suspiciousPersonInfoDTO = bundle.getParcelable(ConstantUtil.SUSPICIOUS_PERSON_INFO_BUNDLE_KEY);
            if(suspiciousPersonInfoDTO != null){
                InformationAboutSuspuciousPerson informationAboutSuspuciousPerson = new InformationAboutSuspuciousPerson(suspiciousPersonInfoDTO);
                suspiciousActivityReport.setInformationAboutSuspuciousPerson(informationAboutSuspuciousPerson);
            }
        }
        if(bundle.containsKey(ConstantUtil.SUSPICIOUS_VEHICLE_INFO_BUNDLE_KEY)){
            SuspiciousVehicleInfoDTO suspiciousVehicleInfoDTO = bundle.getParcelable(ConstantUtil.SUSPICIOUS_VEHICLE_INFO_BUNDLE_KEY);
            if(suspiciousVehicleInfoDTO != null){
                VehicleInformation vehicleInformation = new VehicleInformation(suspiciousVehicleInfoDTO);
                suspiciousActivityReport.setVehicleInformation(vehicleInformation);
            }
        }
        if(bundle.containsKey(ConstantUtil.SUSPICIOUS_INCIDENT_INFO_BUNDLE_KEY)){
            SuspiciousIncidentInfoDTO suspiciousIncidentInfoDTO = bundle.getParcelable(ConstantUtil.SUSPICIOUS_INCIDENT_INFO_BUNDLE_KEY);
            if(suspiciousIncidentInfoDTO != null){
                SuspiciousIncidentOrEvent suspiciousIncidentOrEvent = new SuspiciousIncidentOrEvent(suspiciousIncidentInfoDTO);
                suspiciousActivityReport.setSuspiciousIncidentOrEvent(suspiciousIncidentOrEvent);
            }
        }
        return suspiciousActivityReport;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(view == null){
            view = inflater.inflate(R.layout.fragment_suspicious_incident_or_event, container, false);
            initView(view);
            initListener();
            Bundle arguments = getArguments();
            if(arguments.containsKey(ConstantUtil.SUSPICIOUS_INCIDENT_INFO_BUNDLE_KEY)){
                suspiciousIncidentInfoDTO = arguments.getParcelable(ConstantUtil.SUSPICIOUS_INCIDENT_INFO_BUNDLE_KEY);
                if(suspiciousIncidentInfoDTO != null){
                    setData();
                }
            }
            setupDatePickerDialog(this);
        }
        return view;
    }

    @Override
    protected void initListener() {
        saveButton.setOnClickListener(this);
        prevButton.setOnClickListener(this);
        dateSelectButton.setOnClickListener(this);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (position != 0) {
            NameCodePair nameCodePair = (NameCodePair) parent.getSelectedItem();
            if(nameCodePair.isEventOrIncidentType()) {
                selectedTypeOfIncidentOrEvent = (TypeOfIncidentOrEvent) nameCodePair;
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
