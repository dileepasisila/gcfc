package com.eventqplatform.EQSafety.firebase.dto;

public class ContactType extends  NameCodePair{

    private String typeName;
    private int id;

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return this.typeName;
    }
}
