package com.eventqplatform.EQSafety.firebase.dto;

import com.eventqplatform.EQSafety.util.ConstantUtil;

public class NameCodePair {

    private String code;
    private String name;
    private String type;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return code + " - " + name;
    }

    public boolean isEyeColorType(){
        return ConstantUtil.EYE_COLOR_TYPE.equals(this.type);
    }

    public boolean isEventOrIncidentType(){
        return ConstantUtil.INCIDENT_OR_EVENT_TYPE.equals(this.type);
    }

    public boolean isContactType(){
        return ConstantUtil.CONTACT_TYPE.equals(this.type);
    }

    public boolean isHairColorType(){
        return ConstantUtil.HAIR_COLOR_TYPE.equals(this.type);
    }
}
