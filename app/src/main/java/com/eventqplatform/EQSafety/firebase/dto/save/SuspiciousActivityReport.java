package com.eventqplatform.EQSafety.firebase.dto.save;

public class SuspiciousActivityReport {

    private Contacts contacts;
    private InformationAboutSuspuciousPerson informationAboutSuspuciousPerson;
    private SuspiciousIncidentOrEvent suspiciousIncidentOrEvent;
    private TypeOfInformation typeOfInformation;
    private VehicleInformation vehicleInformation;

    public Contacts getContacts() {
        return contacts;
    }

    public void setContacts(Contacts contacts) {
        this.contacts = contacts;
    }

    public InformationAboutSuspuciousPerson getInformationAboutSuspuciousPerson() {
        return informationAboutSuspuciousPerson;
    }

    public void setInformationAboutSuspuciousPerson(InformationAboutSuspuciousPerson informationAboutSuspuciousPerson) {
        this.informationAboutSuspuciousPerson = informationAboutSuspuciousPerson;
    }

    public SuspiciousIncidentOrEvent getSuspiciousIncidentOrEvent() {
        return suspiciousIncidentOrEvent;
    }

    public void setSuspiciousIncidentOrEvent(SuspiciousIncidentOrEvent suspiciousIncidentOrEvent) {
        this.suspiciousIncidentOrEvent = suspiciousIncidentOrEvent;
    }

    public TypeOfInformation getTypeOfInformation() {
        return typeOfInformation;
    }

    public void setTypeOfInformation(TypeOfInformation typeOfInformation) {
        this.typeOfInformation = typeOfInformation;
    }

    public VehicleInformation getVehicleInformation() {
        return vehicleInformation;
    }

    public void setVehicleInformation(VehicleInformation vehicleInformation) {
        this.vehicleInformation = vehicleInformation;
    }
}
