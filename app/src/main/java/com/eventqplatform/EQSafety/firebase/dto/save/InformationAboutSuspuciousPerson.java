package com.eventqplatform.EQSafety.firebase.dto.save;

import com.eventqplatform.EQSafety.dto.SuspiciousPersonInfoDTO;

public class InformationAboutSuspuciousPerson {

    private String address;
    private String complexionRaceOrEthnicity;
    private String dateOfBirth;
    private String eyeColor;
    private String gender;
    private String hairColor;
    private String height;
    private String name;
    private String otherNames;
    private String phoneNumber;
    private String scarsMarksTattoosAccentsHabits;
    private String weight;

    public InformationAboutSuspuciousPerson(){

    }

     public InformationAboutSuspuciousPerson(SuspiciousPersonInfoDTO suspiciousPersonInfoDTO){
        this.address = suspiciousPersonInfoDTO.getAddress();
        this.complexionRaceOrEthnicity =suspiciousPersonInfoDTO.getRaceEthnicity();
        this.dateOfBirth = suspiciousPersonInfoDTO.getDob();
        this.eyeColor = suspiciousPersonInfoDTO.getEyeColor();
        this.gender = suspiciousPersonInfoDTO.getGender();
        this.hairColor = suspiciousPersonInfoDTO.getHairColor();
        this.height = suspiciousPersonInfoDTO.getHeight();
        this.name = suspiciousPersonInfoDTO.getName();
        this.otherNames = suspiciousPersonInfoDTO.getOtherName();
        this.phoneNumber = suspiciousPersonInfoDTO.getPhoneNumber();
        this.scarsMarksTattoosAccentsHabits = suspiciousPersonInfoDTO.getScarsMarksTattoosEtc();
        this.weight = suspiciousPersonInfoDTO.getWeight();
     }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getComplexionRaceOrEthnicity() {
        return complexionRaceOrEthnicity;
    }

    public void setComplexionRaceOrEthnicity(String complexionRaceOrEthnicity) {
        this.complexionRaceOrEthnicity = complexionRaceOrEthnicity;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEyeColor() {
        return eyeColor;
    }

    public void setEyeColor(String eyeColor) {
        this.eyeColor = eyeColor;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHairColor() {
        return hairColor;
    }

    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOtherNames() {
        return otherNames;
    }

    public void setOtherNames(String otherNames) {
        this.otherNames = otherNames;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getScarsMarksTattoosAccentsHabits() {
        return scarsMarksTattoosAccentsHabits;
    }

    public void setScarsMarksTattoosAccentsHabits(String scarsMarksTattoosAccentsHabits) {
        this.scarsMarksTattoosAccentsHabits = scarsMarksTattoosAccentsHabits;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}
