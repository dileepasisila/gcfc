package com.eventqplatform.EQSafety.firebase.dto.save;

import com.eventqplatform.EQSafety.dto.SuspiciousIncidentInfoDTO;

public class SuspiciousIncidentOrEvent {

    private String date;
    private String location;
    private String time;
    private String typeOfIncidentOrEvent;
    private String whatDidYouSeeOrHear;
    private String whyDoYouBelieveThisIsUnusualOrSuspicious;

    public SuspiciousIncidentOrEvent(){

    }

    public SuspiciousIncidentOrEvent(SuspiciousIncidentInfoDTO suspiciousIncidentInfoDTO){
        this.date = suspiciousIncidentInfoDTO.getDate();
        this.location = suspiciousIncidentInfoDTO.getLocation();
        this.time = suspiciousIncidentInfoDTO.getTime();
        this.typeOfIncidentOrEvent = suspiciousIncidentInfoDTO.getSuspiciousIncidentType();
        this.whatDidYouSeeOrHear = suspiciousIncidentInfoDTO.getSeeOrHearEvidence();
        this.whyDoYouBelieveThisIsUnusualOrSuspicious = suspiciousIncidentInfoDTO.getUnusualOrSuspiciousReason();
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTypeOfIncidentOrEvent() {
        return typeOfIncidentOrEvent;
    }

    public void setTypeOfIncidentOrEvent(String typeOfIncidentOrEvent) {
        this.typeOfIncidentOrEvent = typeOfIncidentOrEvent;
    }

    public String getWhatDidYouSeeOrHear() {
        return whatDidYouSeeOrHear;
    }

    public void setWhatDidYouSeeOrHear(String whatDidYouSeeOrHear) {
        this.whatDidYouSeeOrHear = whatDidYouSeeOrHear;
    }

    public String getWhyDoYouBelieveThisIsUnusualOrSuspicious() {
        return whyDoYouBelieveThisIsUnusualOrSuspicious;
    }

    public void setWhyDoYouBelieveThisIsUnusualOrSuspicious(String whyDoYouBelieveThisIsUnusualOrSuspicious) {
        this.whyDoYouBelieveThisIsUnusualOrSuspicious = whyDoYouBelieveThisIsUnusualOrSuspicious;
    }
}
