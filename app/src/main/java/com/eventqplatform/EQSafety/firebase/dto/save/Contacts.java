package com.eventqplatform.EQSafety.firebase.dto.save;

import com.eventqplatform.EQSafety.dto.YourContactInfoDTO;

public class Contacts {

    private String address;
    private String contactType;
    private String email;
    private String firstName;
    private String lastName;
    private String phoneNumber;

    public Contacts(){

    }

    public Contacts(YourContactInfoDTO yourContactInfoDTO){
        this.address = yourContactInfoDTO.getAddress();
        this.contactType = yourContactInfoDTO.getContactType();
        this.email = yourContactInfoDTO.getEmail();
        this.firstName = yourContactInfoDTO.getFirstName();
        this.lastName = yourContactInfoDTO.getLastName();
        this.phoneNumber = yourContactInfoDTO.getPhoneNumber();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
