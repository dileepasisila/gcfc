package com.eventqplatform.EQSafety.firebase.dto.save;

import com.eventqplatform.EQSafety.dto.TypeOfInformationDTO;

public class TypeOfInformation {

    private String howDidYouObtainThisInformation;
    private String typeOfSuspicious;

    public TypeOfInformation(){

    }

    public TypeOfInformation(TypeOfInformationDTO typeOfInformationDTO){
        this.howDidYouObtainThisInformation = typeOfInformationDTO.getInformationObservedWay();
        this.typeOfSuspicious = typeOfInformationDTO.getTypeOfSuspiciousActivity();
    }

    public String getHowDidYouObtainThisInformation() {
        return howDidYouObtainThisInformation;
    }

    public void setHowDidYouObtainThisInformation(String howDidYouObtainThisInformation) {
        this.howDidYouObtainThisInformation = howDidYouObtainThisInformation;
    }

    public String getTypeOfSuspicious() {
        return typeOfSuspicious;
    }

    public void setTypeOfSuspicious(String typeOfSuspicious) {
        this.typeOfSuspicious = typeOfSuspicious;
    }
}
