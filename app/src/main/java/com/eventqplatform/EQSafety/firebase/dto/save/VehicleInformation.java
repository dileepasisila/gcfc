package com.eventqplatform.EQSafety.firebase.dto.save;

import com.eventqplatform.EQSafety.dto.SuspiciousVehicleInfoDTO;

public class VehicleInformation {

    private String licensePlateNumberAndState;
    private String vehicleBodyStyle;
    private String vehicleColor;
    private String vehicleMake;
    private String vehicleYear;

    public VehicleInformation(){

    }

    public VehicleInformation(SuspiciousVehicleInfoDTO suspiciousVehicleInfoDTO){
        this.licensePlateNumberAndState = suspiciousVehicleInfoDTO.getLicensePlateState();
        this.vehicleBodyStyle = suspiciousVehicleInfoDTO.getVehicleBody();
        this.vehicleColor = suspiciousVehicleInfoDTO.getVehicleColor();
        this.vehicleMake = suspiciousVehicleInfoDTO.getVehicleMake();
        this.vehicleYear = suspiciousVehicleInfoDTO.getVehicleYear();
    }

    public String getLicensePlateNumberAndState() {
        return licensePlateNumberAndState;
    }

    public void setLicensePlateNumberAndState(String licensePlateNumberAndState) {
        this.licensePlateNumberAndState = licensePlateNumberAndState;
    }

    public String getVehicleBodyStyle() {
        return vehicleBodyStyle;
    }

    public void setVehicleBodyStyle(String vehicleBodyStyle) {
        this.vehicleBodyStyle = vehicleBodyStyle;
    }

    public String getVehicleColor() {
        return vehicleColor;
    }

    public void setVehicleColor(String vehicleColor) {
        this.vehicleColor = vehicleColor;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleYear() {
        return vehicleYear;
    }

    public void setVehicleYear(String vehicleYear) {
        this.vehicleYear = vehicleYear;
    }
}
